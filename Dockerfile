# Set the base image to Ubuntu
FROM ubuntu:14.04

# File Author / Maintainer
MAINTAINER Krishna Aradhi

# Update the sources list
RUN apt-get update

# Install basic applications
RUN apt-get install -y tar git curl build-essential

# Install Python and Basic Python Tools
RUN apt-get install -y python python-dev python-distribute python-pip

# Copy the application folder inside the container
ADD . /crawler

# Get pip to download and install requirements:
RUN pip install -r /crawler/requirements.txt

# Set the default directory where CMD will execute
WORKDIR /crawler

# Set the default command to execute
# when creating a new container
# i.e. using CherryPy to serve the application
# CMD python server.py &
