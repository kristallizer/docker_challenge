import os
import urllib
import re
from bs4 import BeautifulSoup
from flask import Flask, request, jsonify
from celery_init import make_celery
from flask_redis import Redis


app = Flask(__name__)
broker_url = "redis://{0}:{1}/0".format(os.environ["REDIS_PORT_6379_TCP_ADDR"],
                                        os.environ["REDIS_PORT_6379_TCP_PORT"])
app.config.update(
    REDIS_URL=broker_url,
    CELERY_BROKER_URL=broker_url,
    CELERY_RESULT_BACKEND=broker_url
)
redis_store = Redis(app)

celery = make_celery(app)
celery.conf.update(CELERY_ACCEPT_CONTENT=['json'],
                   CELERY_TASK_SERIALIZER='json',
                   CELERY_RESULT_SERIALIZER='json')


@celery.task()
def crawl(url):
    main_page = BeautifulSoup(urllib.urlopen(url).read())
    result = []
    # Look for <img> tags on the main page
    for tag in main_page.find_all('img', src=True):
        result.append(str(tag["src"]))
    all_urls = main_page.find_all('a', href=True)

    for url in all_urls:
        href = url.get('href')
        if href.startswith('http'):
            # Look for <a> tags pointing to an image; yes, there a few of those
            if href.endswith(('jpg', 'png', 'gif')):
                result.append(str(href))
            else:
                try:
                    inner_page = BeautifulSoup(urllib.urlopen(href).read())

                    # Look for <img> tags in the inner page
                    for tag in inner_page.find_all('img', src=True):
                        result.append(str(tag['src']))

                    inner_urls = inner_page.find_all('a', href=True)
                    for inner_url in inner_urls:
                        inner_href = inner_url.get('href')
                        # Look for <a> tags pointing to an image
                        if inner_href and inner_href.startswith('http'):
                            if inner_href.endswith(('jpg', 'png', 'gif')):
                                result.append(str(inner_href))
                # TODO: Record which URLs failed so that they can be returned
                # in the job status response
                except:
                    continue
    return result


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    url_regex = re.compile('/http')
    urls = url_regex.sub(' http', request.values.items()[0][0]).split(' ')

    task_ids = []
    latest_job_id = redis_store.incr('latest_job_id')
    job_id = "job:" + str(latest_job_id)
    for url in urls:
        # task = crawl.delay(url, task_id=str(latest_job_id) + ":" + url)
        task = crawl.apply_async((url,), task_id=str(latest_job_id) + ":" + url)
        task_ids.append(str(task))
        redis_store.rpush(job_id, str(task))

    if len(task_ids) > 0:
        response = jsonify(status="success",
                           job_id=latest_job_id)
        response.status_code = 200
        return response
    else:
        response = jsonify(status="failed") 
        response.status_code = 400
        return response


@app.route('/status/<int:job_id>')
def status(job_id):
    result = {'completed': 0, 'inprogress': 0}
    task_ids = redis_store.lrange('job:' + str(job_id), 0, -1)
    for task_id in task_ids:
        task_result = crawl.AsyncResult(task_id)
        if task_result.ready():
            result['completed'] += 1
        else:
            result['inprogress'] += 1
    response = jsonify(result)
    response.status_code = 200
    return response
        

@app.route('/result/<int:job_id>')
def result(job_id):
    task_ids = redis_store.lrange('job:' + str(job_id), 0, -1)
    results = {}
    for task_id in task_ids:
        task_result = crawl.AsyncResult(task_id)
        if not task_result.ready():
            response = jsonify(status="Job in progress")
            return response
        text = task_result.get()
        results[task_id.split(":",1)[1]] = text
    response = jsonify(results)
    response.status_code = 200
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
